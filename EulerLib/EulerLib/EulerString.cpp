#include "Stdafx.h"

public class EulerString
{
	static bool isPalindrome(string input)
	{
		for (int index = 0; index < (input.length / 2); index++)
		{
			if (input[index] != input[input.length - index])
				return false;
		}
		return true;
	}
};