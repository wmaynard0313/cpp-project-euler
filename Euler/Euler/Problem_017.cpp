#include "stdafx.h"
#include "Problems.h"

/*
If the numbers 1 to 5 are written out in words: 
	one, two, three, four, five
Then there are 
	3 + 3 + 5 + 4 + 4 = 19 
letters used in total.

If all the numbers from 1 to 1000 (one thousand) inclusive were 
written out in words, how many letters would be used?


NOTE: Do not count spaces or hyphens. 

For example, 342 (three hundred and forty-two) contains 23 letters and 115 
(one hundred and fifteen) contains 20 letters. The use of "and" 
when writing out numbers is in compliance with British usage.
*/

void Problem_017::solve()
{
	string ones[] = { "", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine" };
	string tens[] = { "", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };
	string teens[] = { "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
	string all[1001];
	int sum = 0;

	int current;

	for (int h = 0; h < 10; h++)
	{
		for (int t = 0; t < 10; t++)
		{
			for (int o = 0; o < 10; o++)
			{
				current = h * 100 + t * 10 + o;

				if (h)
					if (t || o)
						all[current] += ones[h] + "hundredand";
					else
						all[current] += ones[h] + "hundred";
				if (t == 1)
					all[current] += teens[o];
				else
					all[current] += tens[t] + ones[o];				
			}
		}
	}
	all[1001] = "onethousand";

	for (int index = 0; index < 1002; index++)
	{
		sum += all[index].length();
	}
	setAnswer(stringify(&sum, INT));
}