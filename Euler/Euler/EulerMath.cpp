#include "stdafx.h"
#include "EulerMath.h"
#include <math.h>
#include <stack>
#define CHAR_OFFSET 48;

long EulerMath::sum(vector<int> nums)
{
	long sum = 0;
	for (int index = 0; index < nums.size(); index++)
	{
		sum += nums[index];
	}
	return sum;
}

char EulerMath::intToChar(int input)
{
	return input + CHAR_OFFSET;
}
int EulerMath::charToInt(char input)
{
	return input - CHAR_OFFSET;
}

string EulerMath::bigSum(vector<string> * numbers)
{
	stringstream solution;
	vector<int> sum = { 0 };
	int carryover = 0;

	if ((*numbers)[0] == "13")
		int x = 0;

	for (int outdex = 0; outdex < (*numbers).size(); outdex++)
	{
		for (int index = (*numbers)[outdex].length() - 1, i = 0; index >= 0; index--, i++)
		{
			carryover += charToInt((*numbers)[outdex][index]);

			if (i > sum.size() - 1)
				sum.push_back(carryover % 10);
			else if (carryover > 0)
			{
				carryover += sum[i];
				sum[i] = (carryover % 10);
			}
			carryover /= 10;
			if (index == 0)
			{
				while (carryover > 0 && i++ < sum.size() - 1)
				{
					sum[i] += (carryover % 10);
					carryover /= 10;
				}
			}
		}
		while (carryover > 0)
		{
			sum.push_back(carryover % 10);
			carryover /= 10;
		}
	}

	for (int index = sum.size() - 1; index >= 0; index--)
	{
		solution << sum[index];
	}
	
	return solution.str();
}

string intToString(int input)
{
	stringstream ss;
	ss << input;
	return ss.str();
}

string EulerMath::bigProduct(string x, string y)
{
	int index;
	bool positive = true;

	vector<string> products;

	int xlen = x.length();
	int ylen = y.length();

	if (x[0] == '-')
	{
		positive = !positive;
		x = x.substr(1);
	}
	if (y[0] == '-')
	{
		positive = !positive;
		y = y.substr(1);
	}

	for (int outdex = xlen - 1; outdex >= 0; outdex--)
	{
		for (index = ylen - 1; index >= 0; index--)
		{
			string TEMP_X = x;
			string TEMP_Y = y;
			int temp_x = x[outdex] - 48;
			int temp_y = y[index] - 48;

			string s = intToString(charToInt(x[outdex]) * charToInt(y[index]));
			for (int powOfTens = 0; powOfTens < (xlen - outdex) + (ylen - index) - 2; powOfTens++)
			{
				s += '0';
			}
			products.push_back(s);
		}
	}

	return bigSum(&products);
}


unsigned long long EulerMath::factorial(unsigned long long input)
{
	if (input > 1)
		return (input * factorial(input - 1));
	else
		return input;
}
string EulerMath::bigFactorial(int input)
{
	string factorial;
	vector<char> digits = { '1' };
	int carryover = 0;
	
	while (input > 1)
	{
		bigFactorial(input--, &digits, &carryover);
	}

	for (int index = digits.size() - 1; index >= 0; index--)
	{
		factorial += digits[index];
	}
	

	return factorial;
}
void EulerMath::bigFactorial(int input, vector<char> * digits, int * carryover)
{
	int product;

	for (int index = 0; index < (*digits).size(); index++)
	{
		product = (input * (charToInt((*digits)[index]))) + *carryover;
		(*digits)[index] = intToChar(product % 10);
		*carryover = product / 10;
	}
	while (*carryover > 0)
	{
		(*digits).push_back(intToChar(*carryover % 10));
		*carryover /= 10;
	}
}

vector<int> EulerMath::getDistinctPrimeFactors(int input)
{
	vector<int> factors;
	bool added = false;

	for (int outdex = 2; outdex <= input; outdex++)
	{
		added = false;
		while (!(input % outdex))
		{
			input /= outdex;
			if (!added)
			{
				factors.push_back(outdex);
				added = true;
			}
		}		
	}

	return factors;
}
vector<int> EulerMath::getAllPrimeFactors(int input)
{
	vector<int> factors;

	for (int outdex = 2; outdex <= input; outdex++)
	{
		while (!(input % outdex))
		{
			input /= outdex;
			factors.push_back(outdex);
		}
	}

	return factors;
}
vector<int> EulerMath::getDistinctFactors(int input, bool inclusive)
{
	vector<int> factors;
	bool addMe;
	int root = sqrt(input);

	for (int index = 1 + !inclusive; index < root + 1; index++)
	{
		if (!(input % index))
		{
			factors.push_back(index);
			if (root * index != input)
				factors.push_back(input / index);
		}
	}

	return factors;
}

vector<int> EulerMath::set_primes(int limit)
{
	vector<int> list;
	bitset<2000001> primes;
	primes.set(0);
	primes.set(1);
	
	for (int outdex = 2; outdex < limit; outdex += 1)
	{
		if (!primes[outdex])
		{
			list.push_back(outdex);
			for (int index = outdex * 2; index < limit; index += outdex)
			{
				primes.set(index);
			}
		}
	}

	return list;
}

vector<int> EulerMath::get_primes(int count)
{
	vector<int> list = { 2 };
	int num = 3;
	int index;
	bool isPrime;

	while (list.size() < count)
	{
		isPrime = true;
		for (index = 0; isPrime && index < list.size(); index++)
		{
			if (num % list[index] == 0)
				isPrime = false;
		}
		if (isPrime)
			list.push_back(num);
		num += 2;
	}

	return list;
}

void EulerMath::sequence_fib(vector<int> * list, int prev, int next, int limit)
{
	if (next < limit)
	{
		(*list).push_back(next);
		sequence_fib(list, next, prev + next, limit);
	}
}


void EulerMath::fib_sum(int prev, int next, int * sum, int limit)
{
	if (next < limit)
	{
		*sum += next;
		fib_sum(next, prev + next, sum, limit);
	}
}