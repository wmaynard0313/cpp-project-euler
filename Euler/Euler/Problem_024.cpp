#include "stdafx.h"
#include "Problems.h"
#include "EulerMath.h"
#include <vector>
/*
A permutation is an ordered arrangement of objects. For example, 3124 is one possible 
permutation of the digits 1, 2, 3 and 4. If all of the permutations are listed 
numerically or alphabetically, we call it lexicographic order. The lexicographic 
permutations of 0, 1 and 2 are:
	012   021   102   120   201   210

What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?
*/

string removeCharAt(string input, int index)
{
	string output = "";
	for (int i = 0; i < input.length(); i++)
	{
		if (i != index)
			output += input[i];
	}
	return output;
}

void Problem_024::solve()
{
	/* Start high, move low: each new digit has X! possibilities.
		Example:
			1,000,000 / 9! = 2.  2 will be our first digit, then we
			remove (2 * 9!) from our million, and move to the next digit.
	*/
	int termIndex = 999999;
	string nums = "0123456789";
	string result = "";

	for (int index = 9; index > 0; index--)
	{
		int fact = (int)EulerMath::factorial(index);
		int divisor = termIndex / fact;
		termIndex -= fact * divisor;

		result += nums[divisor];
		nums = removeCharAt(nums, divisor);
	}
	result += nums[0];

	setAnswer(result);
}