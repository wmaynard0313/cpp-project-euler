#include "stdafx.h"
#include <ctime>



class Problem
{
	protected:
		enum DataType { SHORT, INT, LONG, LONG_LONG, FLOAT, DOUBLE, CHAR, STRING };
		string answer = "Answer:\t";
		clock_t time = clock();
		float timeTaken;
		
		string getAnswer() { return answer; }; 
		string stringify(void * input, DataType dt);
		void setAnswer(string input) { answer = input; };
		
	public:
		float getTime() {
			timeTaken = (((float)(clock() - time)) / CLOCKS_PER_SEC);
			return timeTaken;
		};
		void solve();
		string toString() { 
			stringstream ss;
			ss << "Answer:\t" << answer << "\nTime:\t" << getTime() << "s";

			return ss.str();
		}
		string getComment()
		{
			return "\t//|\t" + answer + "\t\t|\t" + stringify(&timeTaken, FLOAT) + "s\t|\t";
		};
		Problem(){ answer = "No Answer Set"; };
};
//__________________________________________________________________Answer_________ | __Time___ | __Date Solved________ |
class Problem_001 : public Problem { public: void solve(); };	//|	233168			|	0s		|	2014-01-19			|
class Problem_002 : public Problem { public: void solve(); };	//|	4613732			|	0s		|	2014-01-19			|
class Problem_003 : public Problem { public: void solve(); };	//|	6857			|	0s		|	2014-01-19			|
class Problem_004 : public Problem { public: void solve(); };	//|	906609			|	0.59s	|	2014-01-19			|
class Problem_005 : public Problem { public: void solve(); };	//|	232792560		|	0.093s	|	2014-01-19			|
class Problem_006 : public Problem { public: void solve(); };	//|	25164150		|	0s		|	2014-01-19			|
class Problem_007 : public Problem { public: void solve(); };	//|	104743			|	3.993s	|	2014-01-19			|
class Problem_008 : public Problem { public: void solve(); };	//|	40824			|	0s		|	2014-01-19			|
class Problem_009 : public Problem { public: void solve(); };	//|	31875000		|	0s		|	2014-01-20			|
class Problem_010 : public Problem { public: void solve(); };	//|	142913828922	|	0.56s	|	2014-01-20			|

class Problem_011 : public Problem { public: void solve(); };	//|	70600674		|	0.03s	|	2014-01-22 10:45:08	|
class Problem_012 : public Problem { public: void solve(); };	//|	76576500		|	0.714s	|	2014-01-22 14:36:43	|
class Problem_013 : public Problem { public: void solve(); };	//|	5537376230		|	0.045s	|	2014-01-22 15:00:08	|
class Problem_014 : public Problem { public: void solve(); };	//|	837799			|	0.196s	|	2014-01-23 08:45:07	|
class Problem_015 : public Problem { public: void solve(); };	//|	137846528820	|	0.036s	|	2014-01-23 12:15:06	|
class Problem_016 : public Problem { public: void solve(); };	//|	1366			|	11.248s	|	2014-01-23 12:23:08	|
							// After creating a BigInt class:	//|	1366			|	0.814s	|	2014-02-07 14:43:12	|
class Problem_017 : public Problem { public: void solve(); };	//|	21124			|	0.071s	|	2014-01-23 13:04:28	|
class Problem_018 : public Problem { public: void solve(); };	//|	1074			|	0.071s	|	2014-01-23 13:38:34	|
class Problem_019 : public Problem { public: void solve(); };	//|	171				|	0.023s	|	2014-01-23 14:49:23	|
class Problem_020 : public Problem { public: void solve(); };	//|	648				|	0.049s	|	2014-01-23 14:52:03	|

class Problem_021 : public Problem { public: void solve(); };	//|	31626			|	0.457s	|	2014-01-23 15:24:27	|
class Problem_022 : public Problem { public: void solve(); };	//|	871198282		|	0.383s	|	2014-01-24 08:46:44	|
class Problem_023 : public Problem { public: void solve(); };	//|	4179871			|	0.881s	|	2014-01-24 09:49:09	|
class Problem_024 : public Problem { public: void solve(); };	//|	2783915460		|	0.025s	|	2014-01-24 11:07:07	|
class Problem_025 : public Problem { public: void solve(); };	//|	4782			|	10.77s	|	2014-01-24 11:38:14	| *had to modify EulerMath::bigSum() due to bugs.
							// After creating a BigInt class:	//|	4782			|	5.468s	|	2014-02-07 15:01:20	|
class Problem_026 : public Problem { public: void solve(); };
class Problem_027 : public Problem { public: void solve(); };
class Problem_028 : public Problem { public: void solve(); };
class Problem_029 : public Problem { public: void solve(); };
class Problem_030 : public Problem { public: void solve(); };

class Problem_031 : public Problem { public: void solve(); };
class Problem_032 : public Problem { public: void solve(); };
class Problem_033 : public Problem { public: void solve(); };
class Problem_034 : public Problem { public: void solve(); };
class Problem_035 : public Problem { public: void solve(); };
class Problem_036 : public Problem { public: void solve(); };
class Problem_037 : public Problem { public: void solve(); };
class Problem_038 : public Problem { public: void solve(); };
class Problem_039 : public Problem { public: void solve(); };
class Problem_040 : public Problem { public: void solve(); };

class Problem_041 : public Problem { public: void solve(); };
class Problem_042 : public Problem { public: void solve(); };
class Problem_043 : public Problem { public: void solve(); };
class Problem_044 : public Problem { public: void solve(); };
class Problem_045 : public Problem { public: void solve(); };
class Problem_046 : public Problem { public: void solve(); };
class Problem_047 : public Problem { public: void solve(); };
class Problem_048 : public Problem { public: void solve(); };
class Problem_049 : public Problem { public: void solve(); };
class Problem_050 : public Problem { public: void solve(); };

class Problem_051 : public Problem { public: void solve(); };
class Problem_052 : public Problem { public: void solve(); };
class Problem_053 : public Problem { public: void solve(); };
class Problem_054 : public Problem { public: void solve(); };
class Problem_055 : public Problem { public: void solve(); };
class Problem_056 : public Problem { public: void solve(); };
class Problem_057 : public Problem { public: void solve(); };
class Problem_058 : public Problem { public: void solve(); };
class Problem_059 : public Problem { public: void solve(); };
class Problem_060 : public Problem { public: void solve(); };

class Problem_061 : public Problem { public: void solve(); };
class Problem_062 : public Problem { public: void solve(); };
class Problem_063 : public Problem { public: void solve(); };
class Problem_064 : public Problem { public: void solve(); };
class Problem_065 : public Problem { public: void solve(); };
class Problem_066 : public Problem { public: void solve(); };
class Problem_067 : public Problem { public: void solve(); };	//|	7273			|	0.075s	|	2014-01-23 14:18:19	|
class Problem_068 : public Problem { public: void solve(); };
class Problem_069 : public Problem { public: void solve(); };
class Problem_070 : public Problem { public: void solve(); };

class Problem_071 : public Problem { public: void solve(); };
class Problem_072 : public Problem { public: void solve(); };
class Problem_073 : public Problem { public: void solve(); };
class Problem_074 : public Problem { public: void solve(); };
class Problem_075 : public Problem { public: void solve(); };
class Problem_076 : public Problem { public: void solve(); };
class Problem_077 : public Problem { public: void solve(); };
class Problem_078 : public Problem { public: void solve(); };
class Problem_079 : public Problem { public: void solve(); };
class Problem_080 : public Problem { public: void solve(); };

class Problem_081 : public Problem { public: void solve(); };
class Problem_082 : public Problem { public: void solve(); };
class Problem_083 : public Problem { public: void solve(); };
class Problem_084 : public Problem { public: void solve(); };
class Problem_085 : public Problem { public: void solve(); };
class Problem_086 : public Problem { public: void solve(); };
class Problem_087 : public Problem { public: void solve(); };
class Problem_088 : public Problem { public: void solve(); };
class Problem_089 : public Problem { public: void solve(); };
class Problem_090 : public Problem { public: void solve(); };

class Problem_091 : public Problem { public: void solve(); };
class Problem_092 : public Problem { public: void solve(); };
class Problem_093 : public Problem { public: void solve(); };
class Problem_094 : public Problem { public: void solve(); };
class Problem_095 : public Problem { public: void solve(); };
class Problem_096 : public Problem { public: void solve(); };
class Problem_097 : public Problem { public: void solve(); };
class Problem_098 : public Problem { public: void solve(); };
class Problem_099 : public Problem { public: void solve(); };
class Problem_100 : public Problem { public: void solve(); };