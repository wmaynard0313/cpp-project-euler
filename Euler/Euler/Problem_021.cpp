﻿#include "stdafx.h"
#include "Problems.h"
#include <bitset>
#include "EulerMath.h"

/*
Let d(n) be defined as the sum of proper divisors of n 
(numbers less than n which divide evenly into n).

If d(a) = b and d(b) = a, where a ≠ b, then a and b are an amicable pair
and each of a and b are called amicable numbers.

For example, the proper divisors of 220 are 
	1, 2, 4, 5, 10, 11, 20, 22, 44, 55 and 110
therefore d(220) = 284. The proper divisors of 284 are 1, 2, 4, 71 and 142; so d(284) = 220.

Evaluate the sum of all the amicable numbers under 10000.
*/

void Problem_021::solve()
{
	bitset<10000> amicables;
	long sumA, sumB;
	int solution = 0;

	// It's dumb that 1 is included as a factor of 220, but not 220.
	// Just add 1 to summations so we don't need to write a new
	// function.

	vector<int> d0 = EulerMath::getDistinctFactors(28, false);
	int sum = EulerMath::sum(d0) + 1;

	for (int index = 2; index < 10000; index++)
	{
		if (!amicables[index])
		{
			sumA = EulerMath::sum(EulerMath::getDistinctFactors(index, false)) + 1;
			if (sumA < 10000)
				sumB = EulerMath::sum(EulerMath::getDistinctFactors(sumA, false)) + 1;
			else sumB = 0;

			if (index != sumA && index == sumB)
			{
				amicables.set(sumA);
				amicables.set(sumB);
			}
		}		
	}

	for (int index = 0; index < amicables.size(); index++)
	{
		if (amicables[index])
			solution += index;
	}

	setAnswer(stringify(&solution, INT));
}