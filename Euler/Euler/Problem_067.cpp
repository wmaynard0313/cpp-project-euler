#include "stdafx.h"
#include "Problems.h"
#include <iostream>
#include <fstream>
#include <vector>

/* 
By starting at the top of the triangle below and moving to adjacent 
numbers on the row below, the maximum total from top to bottom is 23.

   3
  7 4
 2 4 6
8 5 9 3

That is:
	3 + 7 + 4 + 9 = 23.

Find the maximum total from top to bottom in triangle.txt (right click and 
'Save Link/Target As...'), a 15K text file containing a triangle with one-hundred rows.

NOTE: This is a much more difficult version of Problem 18. It is not possible to try 
every route to solve this problem, as there are 2^99 altogether! If you could check one
trillion (10^12) routes every second it would take over twenty billion years to check 
them all. There is an efficient algorithm to solve it. ;o)
*/

void Problem_067::solve()
{
	string line;
	ifstream txt("Problem_067 - Triangle.txt");

	vector<vector<int>> triangle;
	vector<int> * tLine;

	if (txt.is_open())
	{
		while (getline(txt, line))
		{
			triangle.push_back({});
			tLine = &triangle[triangle.size() - 1];
			for (int index = 0; index < line.length(); index += 3)
			{
				(*tLine).push_back(((line[index] - 48) * 10 + (line[index + 1] - 48)));
			}
		}
	}

	// Below is code taken directly from Problem_018 - no modifications.

	int largest = 0;

	for (int row = 1; row < triangle.size(); row++)
	{
		triangle[row][0] += triangle[row - 1][0];
		triangle[row][row] += triangle[row - 1][row - 1];
	}
	for (int row = 2; row < triangle.size(); row++)
	{
		for (int col = 1; col < triangle[row].size() - 1; col++)
		{
			int * cell = &triangle[row - 1][col - 1];

			if (*cell > *(cell + 1))
				triangle[row][col] += *cell;
			else
				triangle[row][col] += *(cell + 1);
		}
	}

	for (int index = 0; index < triangle[triangle.size() - 1].size(); index++)
	{
		if (triangle[triangle.size() - 1][index] > largest)
			largest = triangle[triangle.size() - 1][index];
	}

	setAnswer(stringify(&largest, INT));
}