﻿#include "stdafx.h"
#include "Problems.h"

/*
The sum of the squares of the first ten natural numbers is:
	1^2 + 2^2 + ... + 10^2 = 385

The square of the sum of the first ten natural numbers is:
	(1 + 2 + ... + 10)^2 = 55^2 = 3025

Hence the difference between the sum of the squares of the 
first ten natural numbers and the square of the sum is:
	3025 − 385 = 2640.

Find the difference between the sum of the squares of the 
first one hundred natural numbers and the square of the sum.
*/

void Problem_006::solve()
{
	// Sum of squares of natural numbers is: n * (n + 1) * (2n + 1) / 6.
	// Square of sum of natural numbers is:  (n * (n + 1) / 2) ^ 2.

	int sum_square = 100 * 101 * 201 / 6;
	int square_sum = 100 * 101 / 2;
	square_sum *= square_sum;

	int solution = square_sum - sum_square;
	setAnswer(stringify(&solution, INT));
}