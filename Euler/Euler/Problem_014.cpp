﻿#include "stdafx.h"
#include "Problems.h"
#include <vector>

/*
The following iterative sequence is defined for the set of positive integers:

n → n/2 (n is even)
n → 3n + 1 (n is odd)

Using the rule above and starting with 13, we generate the following sequence:

13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1
It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. 
Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.

Which starting number, under one million, produces the longest chain?

NOTE: Once the chain starts the terms are allowed to go above one million.
*/
int map(vector<int> * values, long long input, int buffer)
{
	long long next;

	if (!(input % 2))
		next = input / 2;
	else if (input > 1)
		next = (input * 3) + 1;

	if (input < (*values).size())
	{
		if ((*values)[input])
			return (*values)[input] + 1 + buffer;
		else
		{
			// The buffer should only be used for numbers above the size.
			(*values)[input] = map(values, next, 0);
			return (*values)[input] + 1 + buffer;
		}
	}
	else
		return map(values, next, ++buffer);	
}

void Problem_014::solve()
{
	int solution = 0;

	vector<int> values;
	values.resize(1000001);
	values[0] = -1;
	values[1] = 1;

	for (int index = 0; index < values.size(); index++)
	{
		if (values[solution] < map(&values, index, 0))
			solution = index;
	}

	setAnswer(stringify(&solution, INT));
}