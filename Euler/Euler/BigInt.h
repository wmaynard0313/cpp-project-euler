/******************************* Class  Overview *******************************\
| Started:		2014-01-24														|
| Updated:		2014-02-07														|
| Editors:		Will Maynard													|
| Description:	C++'s largest integer data type is a 64-bit long.  This is a	|
|				substantial obstacle for many ProjectEuler problems.  Using a	|
|				bitset, we can hold numbers up to (2^(bitset size)) - 1. All	|
|				math functions will require binary mathematics, and operators	|
|				should be overloaded so that a BigInt can be created, compared, |
|				and used in expressions the same way any integer class can, but |
|				also including string representations of a number.				|
\_______________________________________________________________________________/

/********************************** Changelog **********************************\
| 2014-02-07: Added Changelog.													|
| 2014-02-07: Implemented signed capabilities in all math functions.			|
| 2014-02-07: Finally got around to documenting this class.						|
\_______________________________________________________________________________/

/********************************* To-Do  List *********************************\
| 1) Create a derivative class: BigFloat.										|
| 2) Shift Left / Shift Right functions (SHL() & SHR()).						|
| 3) Write more extensive unit tests using random number generators.			|
| 4) Square root functions (sqrt(BigInt)).										|
| 5) pow(BigInt) is likely pretty slow - better options available than			|
|		simple multiplication?													|
| 6) Prime / regular factorization functions.									|
\_______________________________________________________________________________/
*/
#include "stdafx.h"
#include <bitset>

#define MAX_SIZE 5000

class BigInt
{
	private:
		// Variables
		bitset<MAX_SIZE> binary;
		bool hasError = false; 
		bool pos;
		int size;		
		string error;

		// Base conversion functions
		string binaryPow(int input);
		void toBinary(long long input);
		
		// Basic math functions
		BigInt abs(BigInt input);
		void add(BigInt input);
		void sub(BigInt input);
		void pow(BigInt power);
		void mul(BigInt input);
		void div(BigInt input, bool isMod = false);
		short compare(BigInt input);

		// Miscellaneous functions
		static void swap(BigInt * x, BigInt * y);
		static void swap(string * x, string * y);
		void setError(string input); 
		void updateSize();
		
		// Bit-wise functions
		void ROR();
		void ROL();
	public:
		// Constructors
		BigInt(long long input) { toBinary(input); }
		BigInt(bitset<MAX_SIZE> input, bool inPos);
		BigInt(string input);

		// Operator overloads
		// Simple operators
		BigInt operator+(BigInt input) 
		{ 
			BigInt temp = (*this); 
			add(input); 
			swap(this, &temp); 
			return temp; 
		}
		BigInt operator-(BigInt input)
		{
			BigInt temp = (*this);
			sub(input);
			swap(this, &temp);
			return temp;
		}
		BigInt operator*(BigInt input)
		{
			BigInt temp = (*this);
			mul(input);
			swap(this, &temp);
			return temp;
		}
		BigInt operator/(BigInt input)
		{
			BigInt temp = (*this);
			div(input);
			swap(this, &temp);
			return temp;
		}
		BigInt operator%(BigInt input)
		{
			BigInt temp = (*this);
			div(input, true);
			swap(this, &temp);
			return temp;
		}
		BigInt operator^(BigInt input)
		{
			BigInt temp = (*this);
			pow(input);
			swap(this, &temp);
			return temp;
		}

		BigInt operator+(string input){ return (*this) + BigInt(input); }
		BigInt operator-(string input){ return (*this) - BigInt(input); }
		BigInt operator*(string input){ return (*this) * BigInt(input); }
		BigInt operator/(string input){ return (*this) / BigInt(input); }
		BigInt operator%(string input){ return (*this) % BigInt(input); }
		BigInt operator^(string input){ return (*this) ^ BigInt(input); }

		// Complex math operators
		BigInt operator++(){ add(1); return (*this); }
		BigInt operator--(){ sub(1); return (*this); }
		BigInt operator++(int){ add(1); return (*this); }
		BigInt operator--(int){ sub(1); return (*this); }

		void operator=(long long input) { (*this) = BigInt(input); }
		void operator+=(BigInt input){ add(input); }
		void operator-=(BigInt input){ sub(input); }
		void operator*=(BigInt input){ mul(input); }
		void operator/=(BigInt input){ div(input); }
		void operator%=(BigInt input){ div(input, true); }
		void operator^=(BigInt input){ pow(input); }
		void operator<<=(BigInt input){ input++; while (input-- > 0) { ROL(); } }
		void operator>>=(BigInt input){ input++; while (input-- > 0) { ROR(); } }

		void operator=(string input) { (*this) = BigInt(input); }
		void operator+=(string input){ (*this) += BigInt(input); }
		void operator-=(string input){ (*this) -= BigInt(input); }
		void operator*=(string input){ (*this) *= BigInt(input); }
		void operator/=(string input){ (*this) /= BigInt(input); }
		void operator%=(string input){ (*this) %= BigInt(input); }
		void operator^=(string input){ (*this) ^= BigInt(input); }
		void operator<<=(string input){ (*this) <<= BigInt(input); }
		void operator>>=(string input){ (*this) >>= BigInt(input); }

		// Bit-wise operators
		BigInt operator<<(BigInt input){ input++; while (input-- > 0) { ROL(); } return (*this); }
		BigInt operator>>(BigInt input){ input++; while (input-- > 0) { ROR(); } return (*this); }

		BigInt operator<<(string input){ return ((*this) << BigInt(input)); }
		BigInt operator>>(string input){ return ((*this) >> BigInt(input)); }

		// Comparison Operators
		bool operator<(BigInt input) { return (*this).compare(input) < 0; }
		bool operator>(BigInt input) { return (*this).compare(input) > 0; }
		bool operator<=(BigInt input) { return (*this).compare(input) <= 0; }
		bool operator>=(BigInt input) { return (*this).compare(input) >= 0; }
		bool operator==(BigInt input) { return (*this).compare(input) == 0; }
		bool operator!=(BigInt input) { return (*this).compare(input) != 0; }

		bool operator<(string input) { return (*this).compare(BigInt(input)) < 0; }
		bool operator>(string input) { return (*this).compare(BigInt(input)) > 0; }
		bool operator<=(string input) { return (*this).compare(BigInt(input)) <= 0; }
		bool operator>=(string input) { return (*this).compare(BigInt(input)) >= 0; }
		bool operator==(string input) { return (*this).compare(BigInt(input)) == 0; }
		bool operator!=(string input) { return (*this).compare(BigInt(input)) != 0; }

		bool operator<(long long input) { return (*this).compare(BigInt(input)) < 0; }
		bool operator>(long long input) { return (*this).compare(BigInt(input)) > 0; }
		bool operator<=(long long input) { return (*this).compare(BigInt(input)) <= 0; }
		bool operator>=(long long input) { return (*this).compare(BigInt(input)) >= 0; }
		bool operator==(long long input) { return (*this).compare(BigInt(input)) == 0; }
		bool operator!=(long long input) { return (*this).compare(BigInt(input)) != 0; }

		// Advanced math functions
		void factorial(BigInt input);
		
		// Printing functions
		string toDecimal(bool show_sign = false);
		string toBinary(bool show_sign = false);

		static string dec_add(string x, string y);
};