#include "stdafx.h"
#include "Problems.h"
#include "EulerMath.h"
#include <bitset>
#include <vector>

/*
A perfect number is a number for which the sum of its proper divisors is exactly 
equal to the number. For example, the sum of the proper divisors of 28 would be:
	1 + 2 + 4 + 7 + 14 = 28
which means that 28 is a perfect number.

A number n is called deficient if the sum of its proper divisors is less than n 
and it is called abundant if this sum exceeds n.

As 12 is the smallest abundant number:
	1 + 2 + 3 + 4 + 6 = 16
the smallest number that can be written as the sum of two abundant numbers is 24. 
By mathematical analysis, it can be shown that all integers greater than 28123 
can be written as the sum of two abundant numbers. However, this upper limit cannot 
be reduced any further by analysis even though it is known that the greatest number 
that cannot be expressed as the sum of two abundant numbers is less than this limit.

Find the sum of all the positive integers which cannot be written as the sum of two abundant numbers.
*/

void Problem_023::solve()
{
	vector<int> abun;
	bitset<21825> bs;
	int sum = 0;

	for (int index = 2; index < 21825 - 12; index++)
	{
		if (EulerMath::sum(EulerMath::getDistinctFactors(index, false)) + 1 > index)
			abun.push_back(index);
	}
	for (int outdex = 0; outdex < abun.size(); outdex++)
	{
		for (int index = outdex; index < abun.size(); index++)
		{
			int temp = abun[outdex] + abun[index];
			if (temp < 21825)
				bs.set(temp);
		}
	}
	for (int index = 0; index < 21825; index++)
	{
		if (!bs[index])
			sum += index;
	}
	setAnswer(stringify(&sum, INT));
}