﻿#include "stdafx.h"
#include "Problems.h"
#include <vector>
#include "BigInt.h"

/*
The Fibonacci sequence is defined by the recurrence relation:

Fn = Fn−1 + Fn−2, where F1 = 1 and F2 = 1.
Hence the first 12 terms will be:

F1 = 1
F2 = 1
F3 = 2
F4 = 3
F5 = 5
F6 = 8
F7 = 13
F8 = 21
F9 = 34
F10 = 55
F11 = 89
F12 = 144
The 12th term, F12, is the first term to contain three digits.

What is the first term in the Fibonacci sequence to contain 1000 digits?
*/

void Problem_025::solve()
{
	// Adding thousands of BigInts together is expensive - best to just use a string.
	string fibs[] = { "1", "1", "" }; // prev, current, next
	int term = 2;

	while (fibs[1].length() < 1000)
	{
		fibs[2] = BigInt::dec_add(fibs[0], fibs[1]);
		fibs[0] = fibs[1];
		fibs[1] = fibs[2];
		term++;
	}
	setAnswer(stringify(&term, INT));

	/* Old Problem_025 code - prior to BigInt class.
	vector<string> fibs = { "1", "1" };
	int term = 2;

	while (fibs[0].length() < 1000)
	{
		string temp = EulerMath::bigSum(&fibs);
		fibs[1] = fibs[0];
		fibs[0] = temp;
		term++;
	}
	setAnswer(stringify(&term, INT));
	/**/
}