﻿#include "stdafx.h"
#include "Problems.h"
#include "EulerMath.h"

/*
Euler discovered the remarkable quadratic formula:

n² + n + 41

It turns out that the formula will produce 40 primes for the consecutive values n = 0 to 39. 
However, when n = 40, 40² + 40 + 41 = 40(40 + 1) + 41 is divisible by 41, 
and certainly when n = 41, 41² + 41 + 41 is clearly divisible by 41.

The incredible formula  n² − 79n + 1601 was discovered, which produces 80 primes for the 
consecutive values n = 0 to 79. The product of the coefficients, −79 and 1601, is −126479.

Considering quadratics of the form:

n² + an + b, where |a| < 1000 and |b| < 1000

where |n| is the modulus/absolute value of n
e.g. |11| = 11 and |−4| = 4

Find the product of the coefficients, a and b, for the quadratic expression that produces 
the maximum number of primes for consecutive values of n, starting with n = 0.
*/

void Problem_027::solve()
{
	bool notPrime;
	int count;
	int most = 0, hi_a, hi_b;
	int result;

	for (int a = -1000; a < 1000; a++)
	{
		for (int b = -1000; b < 1000; b++)
		{
			notPrime = false;
			count = 0;

			while (!notPrime)
			{
				notPrime = (EulerMath::getDistinctPrimeFactors((count * count) + a * count + b).size() > 1);
				count++;
			}
			if (most < count)
			{
				result = a * b;
				most = count;
			}			
		}
	}
	setAnswer(stringify(&result, INT));

}