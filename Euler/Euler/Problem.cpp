#include "stdafx.h"
#include "Problems.h"
string Problem::stringify(void * input, DataType dt)
{
	stringstream ss;

	switch (dt)
	{
	case SHORT:
		ss << *(short*)input;
		break;
	case INT:
		ss << *(int*)input;
		break;
	case LONG:
		ss << *(long*)input;
		break;
	case LONG_LONG:
		ss << *(long long*)input;
		break;
	case FLOAT:
		ss << *(float*)input;
		break;
	case DOUBLE:
		ss << *(double*)input;
		break;
	case CHAR:
		ss << *(char*)input;
		break;
	case STRING:
		ss << *(string*)input;
		break;
	default:
		ss << "Error: Type Unknown in EulerString::stringify(void*, DataType)\n";
	}
	return ss.str();
}