#include "stdafx.h"
#include "Problems.h"
#include "EulerMath.h"

/*
The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.

Find the sum of all the primes below two million.
*/

void Problem_010::solve()
{
	long long sum = 0;
	vector<int> primes = EulerMath::set_primes(2000000);

	for (int index = 0; index < primes.size(); index++)
	{
		sum += primes[index];
	}
	setAnswer(stringify(&sum, LONG_LONG));
}