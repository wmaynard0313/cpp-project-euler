#include "stdafx.h"
#include "EulerString.h"
#include <set>
#include <algorithm>

void EulerString::alphasort(vector<string> *input)
{
	set<string> list;
	set<string>::iterator iter;
	int index = 0;

	for (index = 0; index < (*input).size(); index++)
	{
		list.insert((*input)[index]);
	}
	for (index = 0, iter = list.begin(); iter != list.end(); iter++)
	{
		(*input)[index++] = *iter;
	}
}

bool EulerString::isPalindrome(string input)
{
	char * ptr = &input[0];
	int offset = input.length() + 1;

	while ((offset -= 2) > 0)
		if (*ptr != *(ptr++ + offset))
			return false;
	return true;
}