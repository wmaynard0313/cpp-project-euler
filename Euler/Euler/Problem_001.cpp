#include "stdafx.h"
#include "Problems.h"

/*
If we list all the natural numbers below 10 that are multiples 
of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.

Find the sum of all the multiples of 3 or 5 below 1000.
*/

void Problem_001::solve()
{
	int sum = 0;
	for (int current = 0; current < 1000; current++)
	{
		if (!(current % 3) || !(current % 5))
			sum += current;
	}
	setAnswer(stringify(&sum, INT));
}