﻿#include "stdafx.h"
#include "Problems.h"
#include "EulerMath.h"

/*
n! means n × (n − 1) × ... × 3 × 2 × 1

For example, 10! = 10 × 9 × ... × 3 × 2 × 1 = 3628800,
and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.

Find the sum of the digits in the number 100!
*/

void Problem_020::solve()
{
	int sum = 0;
	string factorial = EulerMath::bigFactorial(100);
	for (int index = 0; index < factorial.length(); index++)
	{
		sum += (factorial[index] - 48);
	}
	setAnswer(stringify(&sum, INT));
}