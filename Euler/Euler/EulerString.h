#include "stdafx.h"
#include <vector>

class EulerString
{
	public:
		static void alphasort(vector<string> *input);
		static bool isPalindrome(string input);
};