#include "stdafx.h"
#include "Problems.h"

/*
You are given the following information, but you may prefer to do some research for yourself.

1 Jan 1900 was a Monday.
Thirty days has September,
April, June and November.
All the rest have thirty-one,
Saving February alone,
Which has twenty-eight, rain or shine.
And on leap years, twenty-nine.

A leap year occurs on any year evenly divisible by 4, but not on a century unless it is divisible by 400.
How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?
*/

void Problem_019::solve()
{
	enum Month { JAN, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV, DEC };

	int day = (1 + 365) % 7;  // Sunday is 0; our given was in 1900, not 1901.
	int count = 0; // Sep 1 1901 is the first sunday

	for (int year = 1901; year < 2001; year++)
	{
		for (int month = 0; month < 12; month++)
		{
			switch (month)
			{
				case FEB:
					if (!(year % 4))
						day += 29;
					else
						day += 28;
					break;
				case APR:
					day += 30;
					break;
				case JUN:
					day += 30;
					break;
				case SEP:
					day += 30;
					break;
				case NOV:
					day += 30;
					break;
				default:
					day += 31;
			}
			count += (!(day %= 7));
		}
	}
	setAnswer(stringify(&count, INT));
}