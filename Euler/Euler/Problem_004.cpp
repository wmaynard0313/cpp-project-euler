#include "stdafx.h"
#include "Problems.h"
#include "EulerString.h"

/*
A palindromic number reads the same both ways. The largest 
palindrome made from the product of two 2-digit numbers 
is 9009 = 91 � 99.

Find the largest palindrome made from the product of two 3-digit numbers.
*/

void Problem_004::solve()
{
	int product;

	int largest = -1;
	for (int outdex = 100; outdex < 999; outdex++)
	{
		for (int index = outdex; index < 999; index++)
		{
			product = index * outdex;
			if (product > largest && EulerString::isPalindrome(stringify(&product, INT)))
				largest = product;
		}
	}

	setAnswer(stringify(&largest, INT));
}