#include "stdafx.h"
#include "Problems.h"
#include "BigInt.h"

/*
2^15 = 32768 and the sum of its digits is:
	3 + 2 + 7 + 6 + 8 = 26

What is the sum of the digits of the number 2^1000?
*/

void Problem_016::solve()
{
	BigInt big_num = 2;
	big_num ^= 1000;

	string result = big_num.toDecimal();
	int sum = 0;

	for (int index = 0; index < result.length(); index++)
	{
		sum += result[index] - 48;
	}
	setAnswer(stringify(&sum, INT));

	/* Old Problem_016 code - prior to the BigInt class.

	string product = "1";
	int sum = 0;

	for (int index = 0; index < 1000; index++)
	{
		product = EulerMath::bigProduct(product, "2");
	}
	for (int index = 0; index < product.length(); index++)
	{
		sum += product[index] - 48;
	}
	setAnswer(stringify(&sum, INT));
	*/
}