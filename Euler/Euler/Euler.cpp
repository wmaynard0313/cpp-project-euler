#include "stdafx.h"
#include "Problems.h"
#include <vector>

#using <System.dll>
#using <System.Drawing.dll>
#using <System.Windows.Forms.dll>
using namespace System;
using namespace System::Windows::Forms;

using namespace std;
void print(string input);
void copy(string input);
const string getDateTime();
void generateProblemHeaders(int start, int end);

[STAThread] void _tmain(int argc, _TCHAR* argv[])
{
	system("color 1F");
	Problem_027 base;
	base.solve();
	vector<Problem> problems;
	problems.push_back(base);

	stringstream ss;
	ss << "\n\n" <<
		"---------------------------------------------" << "\n\n"
		"Your answer has been copied to the clipboard." << "\n\n"
		"---------------------------------------------" << "\n\n";
	string message = ss.str();

	print(problems[0].toString() + message);
	//generateProblemHeaders(21, 100);
	copy(problems[0].getComment() + getDateTime() + "\t|");
}

void print(string input)
{
	cout << input;
	system("pause");
}

void copy(string input)
{
	Clipboard::SetDataObject(gcnew System::String(input.c_str()), true);
}

const string getDateTime()
{
	time_t     now = time(0);
	struct tm  tstruct;
	char       buf[80];
	tstruct = *localtime(&now);
	strftime(buf, sizeof(buf), "%Y-%m-%d %X", &tstruct);

	return buf;
}

void generateProblemHeaders(int start, int end)
{
	stringstream toCopy;
	for (int index = start; index <= end; index++)
	{
		toCopy << "class Problem_";
		if (index < 100)
			toCopy << "0";
		toCopy << index << " : public Problem{ public: void solve(); };\n";
		if (!(index % 10))
			toCopy << '\n';
	}
	copy(toCopy.str());
}