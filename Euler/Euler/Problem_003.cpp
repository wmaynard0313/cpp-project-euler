#include "stdafx.h"
#include "Problems.h"

/*
The prime factors of 13195 are 5, 7, 13 and 29.

What is the largest prime factor of the number 600851475143 ?
*/

void Problem_003::solve()
{
	long long target = 600851475143;
	int factor = 3;
	int largest = -1;

	while (factor * factor < target)
	{
		if (!(target % factor))
		{
			target /= factor;
			largest = factor;
		}
		factor += 2;
	}
	
	// The value of target by the end of the above loop will be prime.
	setAnswer(stringify(&target, INT));
}