#include "stdafx.h"
#include "Problems.h"
#include <iostream>
#include <fstream>
#include <vector>
#include "EulerString.h"

/*
Using names.txt (right click and 'Save Link/Target As...'), 
a 46K text file containing over five-thousand first names, begin by sorting it 
into alphabetical order. Then working out the alphabetical value for each name, 
multiply this value by its alphabetical position in the list to obtain a name score.

For example, when the list is sorted into alphabetical order, COLIN, which is worth:
	3 + 15 + 12 + 9 + 14 = 53
is the 938th name in the list. So, COLIN would obtain a score of 938 � 53 = 49714.

What is the total of all the name scores in the file?
*/



int score(string input, int position)
{
	int sum = 0;
	for (int index = 0; index < input.length(); index++)
	{
		sum += (input[index] - 64);
	}
	return (sum * position);
}
void Problem_022::solve()
{
	string line;
	ifstream txt("Problem_022 - Names.txt");
	vector<string> names;
	long long sum = 0;
	int count = 0;

	if (txt.is_open())
		getline(txt, line);

	for (int index = 0; index < line.length(); index++)
	{
		if (line[index] == '\"')
		{
			string temp = "";
			while (line[++index] != '\"')
			{
				temp += line[index];
			}
			names.push_back(temp);
		}
	}
	EulerString::alphasort(&names);

	for (int index = 0; index < names.size(); index++)
	{
		sum += score(names[index], index + 1);
	}

	setAnswer(stringify(&sum, LONG_LONG));
}