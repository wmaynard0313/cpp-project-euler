#include "stdafx.h"
#include "TestCase.h"
#include "BigInt.h"

void tc_BigInt::run()
{
	BigInt a = 25;
	BigInt b = 50;

	cout << "Addition\n";
	a += b;	// 75
	cout << a.toDecimal(true) << endl;
	a = -25;
	a += b;	// 25
	cout << a.toDecimal(true) << endl;
	a = -25; b = -50;
	a += b;	// -75
	cout << a.toDecimal(true) << endl;
	a = 25; b = -50;
	a += b; // -25;
	cout << a.toDecimal(true) << endl;

	cout << "Subtraction\n";
	a = 10;	b = 20;
	a -= b; // -10
	cout << a.toDecimal(true) << endl;
	a = -10;
	a -= b; // -30
	cout << a.toDecimal(true) << endl;
	a = -10; b = -20;
	a -= b; // 10
	cout << a.toDecimal(true) << endl;
	a = 10; b = -20;
	a -= b; // 30
	cout << a.toDecimal(true) << endl;

	cout << "Multiplication\n";
	a = 5; b = 10;
	a *= b; // 50
	cout << a.toDecimal(true) << endl;
	a = -5;
	a *= b; // -50
	cout << a.toDecimal(true) << endl;
	a = -5; b = -10;
	a *= b; // 50
	cout << a.toDecimal(true) << endl;
	a = 5; b = -10;
	a *= b; // -50
	cout << a.toDecimal(true) << endl;

	cout << "Division\n";
	a = 100; b = 5;
	a /= b; // 20
	cout << a.toDecimal(true) << endl;
	a = -100;
	a /= b; // -20
	cout << a.toDecimal(true) << endl;
	a = -100; b = -5;
	a /= b; // 20
	cout << a.toDecimal(true) << endl;
	a = 100; b = -5;
	a /= b; // -20
	cout << a.toDecimal(true) << endl;

	cout << "Modulous\n";
	a = 47; b = 13;
	a %= b; // 8
	cout << a.toDecimal(true) << endl;
	a = -47; b = 13;
	a %= b; // 5
	cout << a.toDecimal(true) << endl;
	a = -47; b = -13;
	a %= b; // -8
	cout << a.toDecimal(true) << endl;
	a = 47; b = -13;
	a %= b; // -5
	cout << a.toDecimal(true) << endl;

	cout << "Exponentiation\n";
	a = 5; b = 2;
	a ^= b; // 25
	cout << a.toDecimal(true) << endl;
	a = -5; b = 2;
	a ^= b; // 25
	cout << a.toDecimal(true) << endl;
	a = -5; b = -2;
	a ^= b; // Error, or sqrt(5) * i
	cout << a.toDecimal(true) << endl;
	a = 5; b = -2;
	a ^= b; // sqrt(5)
	cout << a.toDecimal(true) << endl;
}