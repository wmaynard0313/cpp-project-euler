﻿#include "stdafx.h"
#include "Problems.h"
#include "EulerMath.h"

/*
Starting in the top left corner of a 2×2 grid, and only being able to move 
to the right and down, there are exactly 6 routes to the bottom right corner.

╒═══╤═══╗   ╒═══╗───┐   ╒═══╗───┐   ╓───┬───┐   ╓───┬───┐   ╓───┬───┐
│   │   ║   │   ║   │   │   ║   │   ║   │   │   ║   │   │   ║   │   │
├───┼───╢   ├───╚═══╗   ├───╫───┤   ╚═══╪═══╗   ╚═══╗───┤   ╟───┼───┤
│   │   ║   │   │   ║   │   ║   │   │   │   ║   │   ║   │   ║   │   │
└───┴───╜   └───┴───╜   └───╚═══╛   └───┴───╜   └───╚═══╛   ╚═══╧═══╛

How many such routes are there through a 20×20 grid?
*/

void Problem_015::solve()
{
	// n!/ (r!(n - r)!
	// r : grid size
	// n : segments to traverse (2r in this case)

	// 40! / (20! (40 - 20)!) == (40! / (20!^2))

	vector<int> numerator;
	vector<int> denominator;

	// Get vectors of numerator's and denominator's prime factors.
	for (int outdex = 1; outdex <= 40; outdex++)
	{
		vector<int> factors = EulerMath::getAllPrimeFactors(outdex);
		for (int index = 0; index < factors.size(); index++)
		{
			numerator.push_back(factors[index]);
			if (outdex <= 20)
			{
				denominator.push_back(factors[index]);
				denominator.push_back(factors[index]);
			}
		}		
	}
	// Simplify the equation.
	for (int x = 0; x < numerator.size(); x++)
	{
		for (int y = 0; y < denominator.size(); y++)
		{
			int temp_x = numerator[x];
			int temp_y = denominator[y];

			if (numerator[x] == denominator[y])
			{
				numerator[x] = 1;
				denominator[y] = 1;
				break;
			}
		}
	}
	// Evaluate.
	long long num = 1;
	long long den = 1;

	for (int index = 0; index < numerator.size(); index++)
	{
		num *= numerator[index];
	}
	for (int index = 0; index < denominator.size(); index++)
	{
		den *= denominator[index];
	}
	num /= den;

	setAnswer(stringify(&num, LONG_LONG));
}