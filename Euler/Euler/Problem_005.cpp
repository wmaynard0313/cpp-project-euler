#include "stdafx.h"
#include "Problems.h"

/*
2520 is the smallest number that can be divided by 
each of the numbers from 1 to 10 without any remainder.

What is the smallest positive number that is evenly 
divisible by all of the numbers from 1 to 20?
*/

void Problem_005::solve()
{
	bool found = false;
	for (long outdex = 19; !found; outdex += 19)
	{
		found = true;
		for (int index = 20; found && index > 2; index--)
		{
			if (outdex % index > 0)
				found = false;
		}
		if (found)
			setAnswer(stringify(&outdex, LONG));
	}
}