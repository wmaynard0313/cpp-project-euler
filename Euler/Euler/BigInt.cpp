#include "stdafx.h"
#include "BigInt.h"
#include "EulerMath.h"
#include <stack>
#include <queue>

#pragma warning (disable: 4800)

// Visual Studio collapse shortcuts:
// Ctrl + M, Ctrl + H: C++ Equivalent of C#'s #region.
// Ctrl + M, Ctrl + U: Undo the above.

//_______________________________________________________________________________abs(BigInt)
// Returns the absolute value of a BigInt.
BigInt BigInt::abs(BigInt input)
{
	input.pos = true;
	return input;
}
//_______________________________________________________________________________setError(string)
// Sets an error message for the BigInt.
void BigInt::setError(string input)
{
	hasError = true;
	error = input;
}
//_______________________________________________________________________________div(BigInt, bool)
// Divides this by a divisor.  Set isMod for modulous.
void BigInt::div(BigInt divisor, bool isMod)
{
	BigInt buffer = 0;
	BigInt quotient = 0;
	bool inNeg = !divisor.pos;							// Tracks the input number's sign for modulous.
	bool negative = (*this).pos ^ divisor.pos;			// The outcome in division will be negative.
	bool bothNegative = !(*this).pos && !divisor.pos;
	(*this).pos = divisor.pos = true;

	if (divisor == 0)
		setError("Error: cannot divide by zero.");

	// Factor out all of the 2s by shifting and decreasing the size.
	while (!(*this).binary[0] && !divisor.binary[0])
	{
		(*this).ROR();
		divisor.ROR();
		(*this).size--;
		divisor.size--;
	}

	quotient.size = (*this).size;

	// Load binary digits into a buffer.  When buffer is larger than the divisor,
	// subtract the divisor.  This is long division in binary form.  Conveniently,
	// the buffer will act as the remainder for modulous if we need it.
	for (int index = (*this).size - 1; index >= 0; index--)
	{
		buffer.size++;
		buffer.ROL();
		buffer.updateSize();
		buffer.binary[0] = (*this).binary[index];

		if (abs(buffer) >= abs(divisor))
		{
			buffer -= abs(divisor);
			quotient.binary.set(index, 1);
		}
		else
			quotient.binary.set(index, 0);
	}

	quotient.updateSize();

	if (isMod)
	{
		(*this) = buffer;
		if (negative)
		{
			(*this) -= divisor;
			if (!inNeg)
				(*this).pos = !(*this).pos;
		}
		else if (bothNegative)
			(*this).pos = false;
	}
	else
	{
		(*this) = quotient;
		(*this).pos = !negative;
	}
	if (abs(*this) == 0)
		(*this) = 0;
}
//_______________________________________________________________________________compare(BigInt)
// Compare this with another BigInt.  Returns:
//	1: (*this) > input
//	0: (*this) == input
//	-1: (*this) < input
short BigInt::compare(BigInt input)
{
	// I apologize for the confusion below, but it drastically reduces
	// the sheer number of if statements required for this.
	if ((*this).size > input.size || (*this).pos ^ input.pos)
		return 1 - ((*this).pos ? 0 : 2);
	else if ((*this).size < input.size)
		return -1 + ((*this).pos ? 0 : 2);

	for (int index = size - 1; index >= 0; index--)
	{
		short temper = (*this).binary[index] - input.binary[index];
		if (temper != 0)
			return temper;
	}
	return 0;
}
//_______________________________________________________________________________sub(BigInt)
// Subtract another BigInt from this.
void BigInt::sub(BigInt input)
{
	if (!input.pos)
		add(abs(input));
	else if (!(*this).pos && input.pos)
	{
		(*this).pos = true;
		add(input);
		(*this).pos = false;
	}
	else
	{
		short compare = (*this).compare(input);
		if (compare == 0)
			(*this) = 0;
		else
		{
			if (compare < 0)
			{
				swap(this, &input);
				pos = !pos;
			}
			for (int outdex = input.size - 1; outdex >= 0; outdex--)
			{
				if ((*this).binary[outdex] == input.binary[outdex])
					(*this).binary[outdex] = input.binary[outdex] = 0;
				else if ((*this).binary[outdex] < input.binary[outdex])
				{
					bool done = false;
					for (int index = outdex; index < (*this).size && !done; index++)
					{
						done = (*this).binary[index];
						(*this).binary[index] = !(*this).binary[index];
					}
				}
			}
		}
		(*this).updateSize();
	}
}
//_______________________________________________________________________________updateSize()
// Cuts off leading zeroes of a BigInt.
void BigInt::updateSize()
{
	while ((*this).binary[size - 1] == 0 && size > 1)
	{
		size--;
	}
}
//_______________________________________________________________________________binaryPow(int)
// Returns a string of the decimal representation of a binary digit of the BigInt.
//	input: the index of the digit to evaluate.
string BigInt::binaryPow(int input)
{
	string start = "1";
	bool carryover = false;

	while (input-- > 0)
	{
		for (int index = start.length() - 1; index >= 0; index--)
		{
			int product = (start[index] - 48) * 2;
			if (index < (int)start.length() - 1)
				start[index] = ((product % 10) + carryover) + 48;
			else
				start[index] = (product % 10) + 48;
			carryover = (product > 9);
		}
		if (carryover)
			start.insert(0, "1");
	}
	return start;
}
//_______________________________________________________________________________swap(string *, string *)
// Swaps two strings.
void BigInt::swap(string * x, string * y)
{
	string temp = *x;
	*x = *y;
	*y = temp;
}
//_______________________________________________________________________________swap(BigInt *, BigInt *)
// Swaps two BigInts.
void BigInt::swap(BigInt * x, BigInt * y)
{
	BigInt temp = *x;
	*x = *y;
	*y = temp;
}
//_______________________________________________________________________________dec_add(string, string)
// Adds two strings of decimal numbers together.
string BigInt::dec_add(string x, string y)
{
	int carryover = 0;
	unsigned int index;
	stack<char> digits;
	string result = "";

	// Make sure sum is bigger.
	if (x.length() < y.length())
		swap(&x, &y);
	else if (x.length() == y.length() && x.compare(y) < 0)
		swap(&x, &y);

	for (index = 1; index <= y.length(); index++)
	{
		int temp = x[x.length() - index] + y[y.length() - index] + carryover - 96;
		digits.push(temp % 10);
		carryover = temp / 10;
	}
	index--;

	if (carryover)
	{
		// carryover will always be a single digit no larger than 1.
		string temp = "";
		temp += carryover + 48;

		// If x has digits remaining, sum them with the carryover.
		// Else carryover should be appended to the beginning of the number.
		if (x.length() > index)
			result = dec_add(x.substr(0, x.length() - index), temp);
		else
			digits.push(carryover);
	}
	else if (x.length() > index)
		result = x.substr(0, x.length() - index);

	while (digits.size() > 0)
	{
		result += digits.top() + 48;
		digits.pop();
	}
	return result;
}
//_______________________________________________________________________________toDecimal(bool)
// Prepares a BigInt for printing by converting it to decimal form.
// Set show_sign to print as a signed number.
string BigInt::toDecimal(bool show_sign)
{
	if (hasError)
		return error;
	string result = "0";
	for (int index = size - 1; index >= 0; index--)
	{
		if (binary[index])
			result = dec_add(result, binaryPow(index));
	}
	if (show_sign)
		return (((*this).pos ? "+" : "-") + result);
	else
		return result;
}
//_______________________________________________________________________________add(BigInt)
// Adds another BigInt to this.
void BigInt::add(BigInt input)
{
	short carryover = 0;
	bool bothNegative = !(*this).pos && !input.pos;

	if (bothNegative)
		(*this).pos = input.pos = true;
	if ((*this).pos && !input.pos)
		(*this) -= abs(input);
	else if (!(*this).pos && input.pos)
	{
		(*this).pos = true;
		(*this) -= input;
		(*this).pos = !(*this).pos;
	}
	else
	{
		if ((*this).size < input.size)
			swap(this, &input);
		(*this).size++;

		for (int index = 0; index < (*this).size; index++)
		{
			carryover += (*this).binary[index] + input.binary[index];
			(*this).binary[index] = carryover % 2;
			carryover /= 2;
		}
		if (!((*this).binary[size - 1]))
			size--;
	}
	if (bothNegative)
		(*this).pos = false;
}
//_______________________________________________________________________________ROL()
// Rotates (not shifts) the binary digits left.
void BigInt::ROL()
{
	queue<bool> q;
	q.push(binary[0]);

	for (int index = 0; index < size; index++)
	{
		if (index == size - 1)
			binary[0] = q.front();
		else
		{
			q.push(binary[index + 1]);
			binary[index + 1] = q.front();
			q.pop();
		}
	}
}
//_______________________________________________________________________________ROR()
// Rotates (not shifts) the binary digits right.
void BigInt::ROR()
{
	queue<bool> q;
	q.push(binary[size - 1]);

	for (int index = size - 1; index >= 0; index--)
	{
		if (index == 0)
			binary[size - 1] = q.front();
		else
		{
			q.push(binary[index - 1]);
			binary[index - 1] = q.front();
			q.pop();
		}
	}
}
//_______________________________________________________________________________mul(BigInt)
// Multiplies this by a multiplier.
void BigInt::mul(BigInt multiplier)
{
	stack<BigInt> s;
	bool negative = (*this).pos ^ multiplier.pos;
	(*this).pos = multiplier.pos = true;

	if (!multiplier.binary[0] && multiplier.size <= 1)
		(*this) = 0;
	else if (!(*this).binary[0] && size <= 1)
		(*this) = 0;
	else
	{
		if ((*this).size < multiplier.size)
			swap(this, &multiplier);

		for (int outdex = 0; outdex < multiplier.size; outdex++)
		{
			if (multiplier.binary[outdex])
			{
				BigInt temp = (*this);
				temp.size = (*this).size + outdex;

				while (!temp.binary[temp.size - 1])
				{
					temp.ROL();
				}
				s.push(temp);
			}
		}
		(*this) = 0;
		while (s.size() > 0)
		{
			(*this) += s.top();
			s.pop();
		}
	}
	(*this).pos = !negative;
}
//_______________________________________________________________________________pow(BigInt)
// Multiplies this by itself [exponent] times.
// Note:
//	This currently lacks the capability of taking BigInt ^ BigInt.
//	That's too huge currently.
void BigInt::pow(BigInt exponent)
{
	if (!exponent.pos)
	{
		if (!(*this).pos)
			setError("Error: square root of a negative number is not a real number.");
		else
			setError("Error: square root capability not yet implemented in BigInt.");
	}
	else
	{
		BigInt temp = (*this);
		while (exponent-- > 0)
		{
			mul(temp);
		}
	}
}
//_______________________________________________________________________________factorial(BigInt)
// Finds the factorial of a BigInt.
void BigInt::factorial(BigInt input)
{
	(*this) = input;
	while (--input > 0)
	{
		(*this) *= input;
	}
}
//_______________________________________________________________________________toBinary(long long)
// Converts a 64-bit integer to binary and stores it in the bitset.
// Note: the bitset stores binary backwards for readability with size.
void BigInt::toBinary(long long input)
{
	size = 0;
	hasError = false;

	if (input > 0)
		pos = true;
	else if (input == 0)
	{
		pos = true;
		binary.reset();
		size++;
	}
	else
	{
		pos = false;
		input *= -1;
	}

	while (input > 0)
	{
		binary.set(size++, input % 2);
		input /= 2;
	}
}
//_______________________________________________________________________________print()
// Returns a string representation of the binary.
string BigInt::toBinary(bool show_sign)
{
	stringstream output;

	if (show_sign)
		output << ((*this).pos ? "+" : "-");

	for (int index = size - 1; index >= 0; index--)
	{
		output << binary[index];
	}
	return output.str();
}
//_______________________________________________________________________________BigInt(bitset, bool)
// Creates a BigInt given a bitset and sign.
BigInt::BigInt(bitset<MAX_SIZE> input, bool inPos = true)
{
	hasError = false;
	(*this).pos = inPos;
	(*this).binary = input;
	int last = 0;
	for (unsigned int index = 0; index < binary.size(); index++)
	{
		if (binary[index])
			last = index;
	}
	size = last + 1;
}
//_______________________________________________________________________________BigIng(string)
// Creates a BigInt given a decimal number in string form.
BigInt::BigInt(string input)
{
	hasError = false;
	(*this) = 0;

	if (input[0] == '-')
		pos = false;
	else
		pos = true;

	string buffer = "";
	for (unsigned int index = 0; index < input.length(); index++)
	{
		buffer += input[index];
		//cout << "buffer: " << buffer << endl;
		if (buffer.length() == 8)
		{
			(*this) *= 100000000;
			(*this) += atoi(buffer.c_str());
			buffer = "";
			//cout << "Appending " + buffer + "... " <<  (*this).toDecimal() << endl;
		}
	}
	int temp = 1;
	for (unsigned int index = 0; index < buffer.length(); index++)
	{
		temp *= 10;
	}
	(*this) *= temp;
	(*this) += atoi(buffer.c_str());
}