#include "stdafx.h"
#include <bitset>
#include <vector>

class EulerMath
{
	private:
		static void bigFactorial(int input, vector<char> * digits, int * carryover);
		static int charToInt(char input);
		static char intToChar(int input);

	public:
		static long sum(vector<int> nums);
		static string bigSum(vector<string> * numbers);
		static string bigProduct(string x, string y);
		static string bigFactorial(int input);
		static unsigned long long factorial(unsigned long long input);
		static vector<int> set_primes(int limit);
		static vector<int> get_primes(int count);
		static vector<int> getAllPrimeFactors(int input);
		static vector<int> getDistinctPrimeFactors(int input);
		static vector<int> getDistinctFactors(int input, bool inclusive);
		static void sequence_fib(vector<int> * list, int prev, int next, int limit);
		static void fib_sum(int prev, int next, int * sum, int limit);
};